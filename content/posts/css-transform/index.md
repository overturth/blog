+++
title = 'css transform'
date = 2024-04-30T16:39:39+08:00
draft = true
+++

css `transform`属性是一个非常强大的属性，它可以让元素进行旋转、缩放、移动、倾斜等操作，每一种转换都是一个变换函数，同时可以应用多种函数。

常用的函数有：
- `translate(x, y)`：在水平方向和垂直方向上移动元素
- `rotate(angle)`：旋转元素，角度单位为deg，正值为顺时针旋转，负值为逆时针旋转
- `scale(x, y)`：缩放元素，x和y分别为水平和垂直方向的缩放比例

[全部函数列表](https://developer.mozilla.org/zh-CN/docs/Web/CSS/transform)。

`translate`和`rotate`函数在实际的应用中比较常用，例如下面的例子会水平距离

例如
```css
 .debug {
      background-color: rgba(255, 0, 0, 0.1);
      border: 1px solid red;
      width: 100px;
      height: 100px;
    }

    .container {
      overflow: hidden;
      margin: 0 auto;
    }

    .ball {
      width: 40px;
      height: 40px;
      /* 水平方向上移动30px，垂直方向上移动40px，然后旋转45度*/
      transform: translate(30px, 40px) rotate(45deg);
    }
```
它会显示为这样的效果：
![css-transform](./images/01.png)


由于父容器设置的`overflow: hidden`，所以超出父容器的部分会被隐藏。
```css
    .ball {
      width: 40px;
      height: 40px;
      transform: translate(110px, 40px) rotate(45deg);
    }
```
这样在水平方向移动了110px，已经超过了父容器的宽度，所以会被隐藏：
![css-transform](./images/02.png)

**相对于通过`display:none`隐藏元素，`transform: translate(x, y)`可以应用动画效果。**